# README #

### What is this repository for? ###

* Learning CMake best practices

### How do I get set up? ###

* Using Debian Linux (Debian >= 8 has cmake version >= 3.0.2)
* apt-get install cmake
* mkdir build; cd build
* cmake -G "Unix Makefiles" ..

### Who do I talk to? ###

* Repo owner

### References ###

[CMake Guide](https://rix0r.nl/blog/2015/08/13/cmake-guide/)

[Effective Modern CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1)
